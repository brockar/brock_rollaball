﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupController : MonoBehaviour
{
    public Vector3 destination; //direction (relative to start) we go
    private float speed = 0.5f; //multiplier for pingpong function; 1f default in example

    Vector3 beginPos;
    Vector3 endPos;

    // Start is called before the first frame update
    void Start()
    {
        beginPos = transform.position;
        endPos = beginPos + destination;
    }

    // Update is called once per frame
    void Update()
    {
        float progress = Mathf.PingPong(Time.time * speed, 1f); //returns a value moving back & forth (ping-pong) between 0 and 1

        /*
         * Returns a Vector3 between beginPos and endPos and all positions in between (based on progress)
         * 0 for beginPos, 1 for endPos; 0.5 progress would be between the two
         */
        transform.position = Vector3.Lerp(beginPos, endPos, progress);

        /*
         * LESSONS LEARNED:
         * + The "destination" did not work as I intended. I ended up having to 
         * double the value of the place I wanted them to originally go.
         */
    }
}
