﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{

    public float speed = 0; //because this is public, it is editable from Unity
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    private Rigidbody rb; //private by default
    private int count;
    private float movementX;
    private float movementY;

    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        rb = GetComponent<Rigidbody>(); //already attached to player object
        SetCountText();
        winTextObject.SetActive(false);
    }

    private void Update()
    {
        //Per Ian's example: added sprint feature.
        if (Keyboard.current.spaceKey.isPressed)
        {
            speed = 30f;
        }
        else
            speed = 10f;
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement * speed);
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 12) //if max score is hit, they win
        {
            winTextObject.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            //updating score
            other.gameObject.SetActive(false);
            count++;
            SetCountText();

            //then the size is randomly changed: grow or shrink
            int effect = Random.Range(0, 2);
            float sizeChange = .1f;
            switch (effect)
            {
                case 0:
                    transform.localScale += new Vector3(sizeChange, sizeChange, sizeChange);
                    break;
                case 1:
                    transform.localScale -= new Vector3(sizeChange, sizeChange, sizeChange);
                    break;
            }
        }
    }
}
